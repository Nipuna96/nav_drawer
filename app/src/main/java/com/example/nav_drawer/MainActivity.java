package com.example.nav_drawer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout myDrawer;
    ActionBarDrawerToggle myToggle;
    NavigationView navigationView;
    ViewPager viewPager;
    TabLayout myTabLayout;
    PagerAdapter adapter;
    TabItem tab_dashboard, tab_events, tab_contacts, tab_settings, tab_activities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myDrawer = (DrawerLayout) findViewById(R.id.drawer);
        myToggle = new ActionBarDrawerToggle(this, myDrawer, R.string.open, R.string.close);
        navigationView = findViewById(R.id.nav_view);
        myDrawer.addDrawerListener(myToggle);
        myToggle.syncState();
        getSupportFragmentManager().beginTransaction().replace(R.id.viewPager, new dashboard()).commit();
        navigationView.setCheckedItem(R.id.nav_dashboard);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = findViewById(R.id.viewPager);
        myTabLayout = findViewById(R.id.tabLayout);
        tab_dashboard = findViewById(R.id.tab_dashboard);
        tab_events = findViewById(R.id.tab_events);
        tab_contacts = findViewById(R.id.tab_contacts);
        tab_settings = findViewById(R.id.tab_settings);
        tab_activities = findViewById(R.id.tab_activities);

        adapter = new PagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT, myTabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        navigationView.setNavigationItemSelectedListener(this);

        myTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //Toast.makeText(getApplicationContext(),"Hello Javatpoint",Toast.LENGTH_SHORT).show();
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(myTabLayout));
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_dashboard:
                viewPager.setCurrentItem(0);
                break;
            case R.id.nav_events:
                viewPager.setCurrentItem(1);
                break;
            case R.id.nav_contacts:
                viewPager.setCurrentItem(2);
                break;
            case R.id.nav_settings:
                viewPager.setCurrentItem(3);
                break;
            case R.id.nav_activities:
                viewPager.setCurrentItem(4);
                break;
        }
        myDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (myToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

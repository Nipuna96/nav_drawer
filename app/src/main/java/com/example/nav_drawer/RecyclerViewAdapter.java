package com.example.nav_drawer;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.view.menu.MenuView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.myViewHolder> {

    ArrayList<ContactModel> listContacts;

    public RecyclerViewAdapter(ArrayList<ContactModel> listContacts) {
        this.listContacts = listContacts;
    }

    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_item,parent,false);
        return new myViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull myViewHolder holder, int position) {
        Glide.with(holder.img).load(listContacts.get(position).getImage()).transform(new CircleCrop()).into(holder.img);
        //holder.img.setImageResource(listContacts.get(position).getImage());
        holder.name.setText(listContacts.get(position).getName());
        holder.phone.setText(listContacts.get(position).getPhone());
    }

    @Override
    public int getItemCount() {
        return listContacts.size();
    }

    class myViewHolder extends RecyclerView.ViewHolder{

        ImageView img;
        TextView name,phone;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.user);
            name = itemView.findViewById(R.id.user_name);
            phone = itemView.findViewById(R.id.phone_number);
        }
    }
}

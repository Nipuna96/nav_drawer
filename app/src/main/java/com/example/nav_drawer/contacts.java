package com.example.nav_drawer;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class contacts extends Fragment {

    RecyclerView recyclerView;
    ArrayList<ContactModel> listContacts;


    public contacts() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_contacts, container, false);
        recyclerView = v.findViewById(R.id.recycleView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        listContacts = new ArrayList<>();
        listContacts.add(new ContactModel("https://cdn.now.howstuffworks.com/media-content/0b7f4e9b-f59c-4024-9f06-b3dc12850ab7-1920-1080.jpg", "Nipuna Sarathchandra", "0710580906"));
        listContacts.add(new ContactModel("https://image.shutterstock.com/image-photo/handsome-unshaven-young-darkskinned-male-260nw-640011838.jpg", "Sachin Muthumala", "0713482903"));
        listContacts.add(new ContactModel("https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8cGVyc29ufGVufDB8fDB8&ixlib=rb-1.2.1&w=1000&q=80", "Akila Sandaruwan", "0745270382"));
        listContacts.add(new ContactModel("https://s3.amazonaws.com/arc-authors/washpost/e8d90017-3451-40a4-a668-901221acbb76.png", "Tharindu Prabath", "0769301724"));
        listContacts.add(new ContactModel("https://thebodyisnotanapology.com/wp-content/uploads/2018/02/pexels-photo-459947.jpg", "Tharushi Shehara", "0714891493"));
        listContacts.add(new ContactModel("https://www.incimages.com/uploaded_files/image/1920x1080/1725x810_27845.jpg", "Dushan Prabodaka", "0714354863"));
        listContacts.add(new ContactModel("https://qodebrisbane.com/wp-content/uploads/2019/07/This-is-not-a-person-2-1.jpeg", "Dulanji Withanage", "0710319294"));
        listContacts.add(new ContactModel("https://images.pexels.com/photos/614810/pexels-photo-614810.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500", "Malith Sudaraka", "0718420485"));
        listContacts.add(new ContactModel("https://qph.fs.quoracdn.net/main-qimg-88f854779acaf0f1abe66e5f74c312b2.webp", "Ashen Herath", "0713857364"));
        listContacts.add(new ContactModel("https://upload.wikimedia.org/wikipedia/commons/a/a0/Pierre-Person.jpg", "Aruna Gamagedara", "0710859309"));

        recyclerView.setAdapter(new RecyclerViewAdapter(listContacts));

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

}

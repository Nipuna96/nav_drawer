package com.example.nav_drawer;

public class ContactModel {

    //int image;
    String image,name,phone;


    public ContactModel(String image, String name, String phone) {
        this.image = image;
        this.name = name;
        this.phone = phone;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
